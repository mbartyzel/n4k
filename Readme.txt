Przyklad:
https://shopappstorephplib.readthedocs.io/en/latest/library/examples.html#rest-get

Przykład:

 $resource = new \DreamCommerce\ShopAppstoreLib\Resource\Product($client);
    // or
    $resource = $client->products;

    // particular object, with ID=1
    $result = $resource->get(1);

    // list of objects
    $result = $resource->get();

    // list of objects (page 3) with filtering/limiting:
    $result = $resource->filters(array('translations.name' => array('=', 'laptop')))->page(3)->limit(10)->get();

-----------------------------------
Przykład:
https://pastebin.com/4DMPgt9B


$resource = new DreamCommerce\ShopAppstoreLib\Resource\Product($client);

// Poberanie 1 strony
$currentPage = 1;
$result = $resource->page($currentPage)->limit(50)->get();

foreach($result as $product){
	// Pętla po wszystich pierwszych 50 produktach - usuwamy każdy z nich po kolei
    $deleteResult = $resource->delete($product["product_id"]);
}

$currentPage++;

// Pobranie kolejnych stron jeśli istnieją
while($currentPage <= $result->getPageCount()){
  // Pobieramy produkty z kolejnej strony
  $result = $resource->page($currentPage)->limit(50)->get();

  foreach($result as $product){
      // Pętla po 50 produktach z kolejnych stron - usuwamy każdy z nich po kolei
      $deleteResult = $resource->delete($product["product_id"]);
  }

  $currentPage++;
}
