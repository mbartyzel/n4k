<?php
require_once __DIR__ . '/config.php';
require_once __DIR__ . '/vendor/autoload.php';

$data = new Volatile();

$clientName = ($argv[1]) ?? 'default'; //nazwa klienta przekazana w parametrze

/**
 * Worker zbierajacy dane
 * Nadpisuje go by przekazac zmienna na dane
 *
 * Class DataCollector
 */
class ProductsDataCollector extends Worker
{
    private $results; // stores the results here

    public function __construct(Volatile $store, $logFile)
    {
        $this->results = $store;
        $this->logFile = $logFile;
    }

    public function start(int $options = PTHREADS_INHERIT_ALL)
    {
        return parent::start(PTHREADS_INHERIT_NONE);
    }

    /**
     * Zaladuj autoloadera do watkow
     */
    public function run()
    {
        require __DIR__ . '/vendor/autoload.php';
    }
}

/**
 * Class ExampleWork
 */
class GetProductsData extends Threaded
{
    public $result;
    public $params;
    public $filters;
    public $config;

    /**
     * Konstruktor
     *
     * GetProductsData constructor.
     * @param int $page
     * @param array $params
     * @param array $filters
     */
    public function __construct(int $page, array $params, array $filters, array $config)
    {
        $this->params = $params;
        $this->params['page'] = $page;
        $this->filters = $filters;
        $this->config = $config;
    }

    /*
     * Logowanie zdarzen
     */
    private function log($str)
    {
        echo $str . "\n";
        if ($this->worker->logFile) {
            fwrite($this->worker->logFile, $str . "\r\n");
        }
    }


    /**
     * Tu pobieramy dane
     */
    public function run()
    {
        try {
            $api = Api::getInstance();
            $api->setParams($this->config);
            $api->connect();

            $getProducts = $api->showProductsList((array)$this->params, (array)$this->filters);

            if ($this->params['page'] == 1) {
                $fullPageCounter = $getProducts->pages;
                $this->worker->results['productsPageCount'] = $fullPageCounter;
                $this->log('Liczba wszystkich stron:' . $fullPageCounter);
                $this->log('Liczba wszystkich produktów:' . $getProducts->count);

            }

//        $this->log('Data:' . date('d-m-Y H:i:s') . ' ' . ' Strona:' . $this->params['page']);

            //dla kazdej 50-ki produktow
            foreach ($getProducts as $product) {
//            if (($product->product_id == 23019125) || ($product->product_id == 23019126)){
//                $this->log("TEST!!! Produkt : " . $product->product_id . ' ma stan ' . $product->stock->stock);
//            } else{
//                continue;
//            }

                $stockParams = [];
                $stockFilters = [];
                $stockFilters['product_id'] = $product->product_id; //testowy identyfikator produktu
                $stockFilters['extended'] = 1; //tylko warianty
                $stockFilters['active'] = 1; //weryfikuje tylko warianty ze stanem magazynowym aktywnym
                $stockParams['limit'] = 50; //ustale maxymalny limit produktow do pobrania
                $stockParams['page'] = 1; //dla pierwszej strony
                $result = $api->showVariantsProductsList((array)$stockParams, (array)$stockFilters);
                //$this->log("Liczba aktywnych wariantów produktu $product->product_id wynosi: " . $result->count);

                //jesli sa jakies warianty
                if ($result->count > 0) {
                    $wariantPageCounter = $result->pages;
                    $sum = 0;
                    foreach ($result as $row) {
                        //jesli to na pewno magazyn wariantu
                        if ($row->extended) {
                            //jesli stan > 0
                            if ($row->stock > 0) {
                                $sum += $row->stock;
                            } else {
                                if ($row->active) {
                                    $this->log("Należy deaktywować wariant $row->stock_id produktu $product->product_id poniewaz ma stan = 0");
                                    $api->deactivateWariant($row->stock_id);
                                } else {
                                    $this->log("ERROR: Wariant $row->stock_id produktu $product->product_id jest nieaktywny.");
                                }
                            }
                        } else {
                            continue; //pomijamy stany magazynowe produktu glownego
                        }
                    }

                    //jesli jest wiecej niz 1 strona wariantow tj > 50 wariantow
                    if ($wariantPageCounter > 1) {
//                    $this->log("Uwaga: Liczba wszystkich wariantow dla produktu $product->product_id wynosi " . $result->count . ". Iteruje po wszystkich wariantach.");

                        for ($page = 2; $page <= $wariantPageCounter; ++$page) {
                            $stockParams['page'] = $page; //dla pierwszej strony
                            $resultEstra = $api->showVariantsProductsList((array)$stockParams, (array)$stockFilters);
                            foreach ($resultEstra as $rowExtra) {
                                //jesli to na pewno magazyn wariantu
                                if ($rowExtra->extended) {
                                    //jesli stan > 0
                                    if ($rowExtra->stock > 0) {
                                        $sum += $rowExtra->stock;
                                    } else {
                                        if ($rowExtra->active) {
                                            $this->log("Należy deaktywować wariant $row->stock_id produktu $product->product_id poniewaz ma stan = 0.");
                                            $api->deactivateWariant($row->stock_id);
                                        }
                                    }
                                } else {
                                    continue; //pomijamy stany magazynowe produktu glownego
                                }
                            }
                        }
                    }
                } else {
                    //jesli nie ma wariantow to pod zmienna sum podstaw aktualny stan produktu
                    $sum = $product->stock->stock;
                }

                $needUpdate = false;
                $arr = array('stock' => null, 'deactivate' => 0);
                //jesli suma z aktywnych wariantow <> od sumy na produkcie to aktualizuje wartosc na produkcie
                if ($sum <> $product->stock->stock) {
                    $this->log("Należy zaktualizować stan produktu $product->product_id z " . $product->stock->stock . " na $sum");
                    $needUpdate = true;
                    $arr['stock'] = $sum;
                }

                if ($sum == 0) {
                    $this->log("Należy deaktywować produkt $product->product_id bo ma stan 0");
                    $needUpdate = true;
                    $arr['deactivate'] = 1;
                }

                if ($needUpdate) {
                    $api->updateMainStock($product->product_id, $arr['stock'], $arr['deactivate']);
                }

                //Zapisuje kazdy przetworzony identyfikator produktu
                $this->worker->results[$product->product_id] = $sum;
            }
        } catch (Exception $ex) {
            $this->log('ERROR: ' . $ex->getMessage());
//            die($ex->getMessage());
        }
    }
}

/**
 * Main code
 */
if ($clientName) {
    $config = $config[$clientName];
    if (!$config) {
        die('Brak danych konfiguracyjnych danego klienta');
    }
} else {
    die('Nie podano nazwy klienta');
}

//startuje czas wykonania operacji
$start_time = microtime(true);

//otwieram plik_logow
$fp = fopen('logi_' . $clientName . '.txt', 'a+');
fwrite($fp, "\nStart aktualizacji: " . date('d-m-Y H:i:s') . "\n");
$config['fh'] = $fp;

/**
 * Pomocnicze tablice
 */
$params = [];
$filters = [];

//Pobranie liczby wszystkich rekordow
$params['limit'] = 50;
$filters['stock.extended'] = 0; //0 - produkty, 1 - warianty
$filters['translations.pl_PL.active'] = 1; //0 - nieaktywne, 1 -aktywne

//worker na pobranie 50 produktow z pierwszej strony
$task = new GetProductsData(1, $params, $filters, $config);
$worker = new ProductsDataCollector($data, $fp);
$worker->start();
$worker->stack($task);
while ($worker->collect()) ;
$worker->shutdown();

//licze czas na pozostale strony
$start_new_time = microtime(true);

//przekazuje liczbe stron do iterowania
$productsPageCount = $data['productsPageCount'];

//tworze pule workerow ktore zajma sie zadaniami i przekazuje tam obiekt $data na dane
$pool = new Pool($config['maxMainWorkers'], "ProductsDataCollector", [$data, $fp]);
//dla kazdej ze stron z produktami
for ($page = 2; $page <= $productsPageCount; ++$page) {
    $task = new GetProductsData($page, $params, $filters, $config);
    $pool->submit($task);
}
//uruchamiam pule taskow
while ($pool->collect()) ;
$pool->shutdown();

//koniec liczenia czasu
$end_time = microtime(true);


//PODSUMOWANIE
echo "\n=========================================================\n";
$log = "Rekordow przetworzonych: " . count($data) . "\n";
echo $log;

$first_time = ($start_new_time - $start_time);
$full_time = ($end_time - $start_time);
$thread_time = ($end_time - $start_new_time);

$log = "\nCzas przetworzenia pierwszej paczki 50 rekordow = " . $first_time . " sec" . "\n";
echo $log;
fwrite($fp, $log);

$log = "Czas przetworzenia pozostałych rekordow = " . $thread_time . " sec" . "\n";
echo $log;
fwrite($fp, $log);

$log = "Pełny czas operacji = " . $full_time . " sec" . "\n";
echo $log;
fwrite($fp, $log);
fwrite($fp, "\n=========================================================\n");

fclose($fp);

//print_r('<pre>');
//print_r($data);
//print_r('</pre>');