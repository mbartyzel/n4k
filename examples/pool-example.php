<?php

// Store the results here - this must be created in the outermost scope
// since Threaded objects are tied to the thread in which they are created.
$data = new Volatile();

// override worker to store an additional property to hold the results
class DataCollector extends Worker
{
    private $results; // stores the results here

    public function __construct(Volatile $store)
    {
        $this->results = $store;
    }
}

class ExampleWork extends Threaded {
    public $result;
    public $value;

    public function __construct(int $i)
    {
        $this->value = $i;
    }

    public function run() {
        usleep(100000);
        echo "Task: {$this->value}\n";
        $this->worker->results[] = "Task: " . $this->value; // push data to the worker's "results" property
    }
}

$pool = new Pool( 20, "DataCollector", [$data] ); // the new subclass and its constructor args

for ($i = 0; $i < 120; ++$i) {
    $task = new ExampleWork($i);
    $pool->submit($task);
}

while ($pool->collect());
$pool->shutdown();

//var_dump($data); // continue manipulating the collected data here