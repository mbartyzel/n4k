<?php
//wyswietlaj ewentualne bledy
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once __DIR__ . '/vendor/autoload.php';

try {
    $client = \DreamCommerce\ShopAppstoreLib\Client::factory(\DreamCommerce\ShopAppstoreLib\Client::ADAPTER_BASIC_AUTH,
        [
            'entrypoint' => 'http://n4k.shoparena.pl/webapi/rest/',
            'username' => 'mbartyzel',
            'password' => '100%Bartyzel'
        ]);

    //pobieram liste produktow
    $resource = new \DreamCommerce\ShopAppstoreLib\Resource\Product($client);
    $data=array('limit' => 2);
    $result = $resource->get($data);

    foreach($result as $r){
//        var_dump($r->stock);

        printf("#%d - %s\n", $r->product_id, $r->translations->pl_PL->name);
    }

} catch (\DreamCommerce\ShopAppstoreLib\Client\Exception $ex) {
    die('Something went wrong with the Client: ' . $ex->getMessage());
} catch (\DreamCommerce\ShopAppstoreLib\Resource\Exception $ex) {
    die('Check your request: ' . $ex->getMessage());
}

