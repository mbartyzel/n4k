#!/usr/bin/php
<?php
if (extension_loaded("pthreads")) {
    echo "Using pthreads\n";
} else  echo "Using polyfill\n";

//wyswietlaj ewentualne bledy
error_reporting(E_ALL);
ini_set('display_errors', 1);




//kontener na zwrotke z watkow
$data = new Volatile();

class SearchGoogle extends Threaded
{
    public $results;
    public $query;

    public function __construct($query)
    {
        $this->query = $query;
    }

    public function run()
    {
        echo microtime(true).PHP_EOL;
        usleep(200000);
        $this->worker->results[] = $this->query .' ' . rand();
    }
}

/**
 * Worker
 * Class Searcher
 */
class Searcher extends Worker
{
    private $results;


    public function __construct(Volatile $store)
    {
        $this->results = $store;
    }

    public function run()
    {
        echo 'Running '.$this->getStacked().' jobs'.PHP_EOL;
    }


}
//tworze kilka workerow ktore beda dostawac takie same doane do wypisania
for ($i=1; $i<=5; $i++) {
// Stack our jobs on our worker
    $worker[$i] = new Searcher($data);

    $searches = range("A", "Z");
    foreach ($searches as &$search) {
        $search = new SearchGoogle($search);
        $worker[$i]->stack($search);
    }
    $worker[$i]->start();
}
for ($i=1; $i<=5; $i++) {
// Start all jobs
    while ($worker[$i]->collect());
// Join all jobs and close worker
    $worker[$i]->shutdown();
}


print_r('<pre>');
print_r($data);
print_r('</pre>');

//foreach ($worker->data as $html) {
//    echo substr($html, 0, 20).PHP_EOL;
//}