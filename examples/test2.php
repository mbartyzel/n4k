#!/usr/bin/php
<?php
class Task extends Threaded
{
    private $value;

    public function __construct(int $i)
    {
        $this->value = $i;
    }

    public function run()
    {
        usleep(100000);
        echo "Task: {$this->value}\n";
    }
}
//ile watkow utworzy
$pool = new Pool(4);

for ($i = 0; $i < 120; ++$i) {
    $pool->submit(new Task($i));
}

while ($pool->collect());


$pool->shutdown();