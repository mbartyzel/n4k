<?php
//wyswietlaj ewentualne bledy
error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once __DIR__ . '/vendor/autoload.php';

try {
    $client = \DreamCommerce\ShopAppstoreLib\Client::factory(\DreamCommerce\ShopAppstoreLib\Client::ADAPTER_BASIC_AUTH,
        [
            'entrypoint' => 'http://n4k.shoparena.pl/webapi/rest/',
            'username' => 'mbartyzel',
            'password' => '100%Bartyzel'
        ]);

    //odpowiedni resource do pobrania listy zdjec
    $resource = new \DreamCommerce\ShopAppstoreLib\Resource\ProductImage($client);
    $result = $resource->get();


    //iteruje po wszystkich zdjeciach i kasuje kolejno
    foreach ($result as $r) {
        echo '<br/>';
        printf("Istnieje %d - %s - %d\n", $r->gfx_id, $r->name, $r->product_id);
        echo '<br/>';
        echo 'Kasuje zdjecie o takim id: ' . $r->gfx_id;
        echo '<br/>';

        //teraz na tym samym resource akcja kasowania i jesli sie powiedzie to wyswietli komunikat
        if ($resource->delete($r->gfx_id)) {
            echo 'Skasowałem to zdjecie: ' . $r->gfx_id;
        }
    }

} catch (\DreamCommerce\ShopAppstoreLib\Client\Exception $ex) {
    die('Something went wrong with the Client: ' . $ex->getMessage());
} catch (\DreamCommerce\ShopAppstoreLib\Resource\Exception $ex) {
    die('Check your request: ' . $ex->getMessage());
}

