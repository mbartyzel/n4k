<?php
require_once __DIR__ . '/config.php';
require_once __DIR__ . '/vendor/autoload.php';

$clientName = ($argv[1]) ?? 'default'; //nazwa klienta przekazana w parametrze

if ($clientName) {
    $config = $config[$clientName];
    if (!$config) {
        die('Brak danych konfiguracyjnych danego klienta');
    }
} else {
    die('Nie podano nazwy klienta');
}

$api = Api::getInstance();
$api->setParams($config);
$api->connect();

$params = [];
$filters = [];
$params['id'] = 23019127;
$filters['stock.extended'] = 0; //0 - produkty, 1 - warianty
$filters['translations.pl_PL.active'] = 1;
//$params['limit'] = 50;
//$filters['stock.extended'] = 0;
//$filters['stock.active'] = true;

//werfyfikuje ile jest produktow i stron
$product = $api->showProductsList($params, $filters);

$stockParams = [];
$stockFilters = [];
$stockFilters['product_id'] = $product->product_id; //testowy identyfikator produktu
$stockFilters['extended'] = 1; //tylko warianty
$stockFilters['active'] = 1; //weryfikuje tylko warianty ze stanem magazynowym aktywnym
$stockParams['limit'] = 50; //ustale maxymalny limit produktow do pobrania
$stockParams['page'] = 1; //dla pierwszej strony
$result = $api->showVariantsProductsList((array)$stockParams, (array)$stockFilters);

print_r("<pre>");
print_r($result);
print_r("</pre>");



die;
