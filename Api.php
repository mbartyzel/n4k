<?php
//wyswietlaj ewentualne bledy
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

use DreamCommerce\ShopAppstoreLib\Client;
use DreamCommerce\ShopAppstoreLib\Resource\Subscriber;
use DreamCommerce\ShopAppstoreLib\Exception\Exception;
use DreamCommerce\ShopAppstoreLib\Resource\Product;
use DreamCommerce\ShopAppstoreLib\Resource\ProductStock;

class Api
{
    private $entrypoint;
    private $userName;
    private $password;
    private $client;
    private $change = 0;
    private $fh;
    private static $instance = null;

    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    protected function __clone()
    {
    }

    protected function __wakeup()
    {
    }

    /**
     * Ustaw dane do polaczenia
     *
     * @param $params
     */
    public function setParams($params)
    {
        $this->entrypoint = $params['entrypoint'];
        $this->userName = $params['username'];
        $this->password = $params['password'];

        if (isset($params['change'])) {
            $this->change = $params['change'];
        }
        //jesli jest podany wskaznik do pliku
        if (isset($params['fh'])) {
            $this->fh = $params['fh'];
        }
    }

    /**
     * Pobierz dane przekazane do API
     * @return array
     */
    public function getParams()
    {
        return array(
            'entrypoint' => $this->entrypoint,
            'username' => $this->userName,
            'password' => $this->password,
            'change' => $this->change
        );
    }

    public function connect()
    {
        try {
            $options = array(
                'entrypoint' => $this->entrypoint,
                'username' => $this->userName,
                'password' => $this->password
            );

            $this->client = Client::factory(Client::ADAPTER_BASIC_AUTH, $options);
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

    /**
     * Api constructor.
     */
    private function __construct()
    {
    }

    /**
     * Wylistuj istniejacych subscrybentow
     */
    public function showSubscribersList()
    {
        try {
            $resource = new Subscriber($this->client);
            return $resource->get();

        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

    /**
     * Dodaj subscrybenta
     *
     * @param $data
     */
    public function addSubsriber($data)
    {
        try {
            $resource = new Subscriber($this->client);
            return $resource->post($data);
        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

    /**
     * Wylistuj istniejace produkty
     */
    public function showProductsList(array $params = null, array $filters = null)
    {
        try {
            $resource = new Product($this->client);

            if ($filters) {
                $resource->filters($filters);
            }

            if (isset($params['limit'])) {
                $resource = $resource->limit($params['limit']);
            }
            if (isset($params['page'])) {
                $resource = $resource->page($params['page']);
            }
            if (isset($params['id'])) {
                $results = $resource->get($params['id']);
            } else {
                $results = $resource->get();
            }

            return $results;

        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }


    /**
     * Wylistuj istniejace produkty
     */
    public function showProduct($id)
    {
        try {
            $resource = new Product($this->client);
            $results = $resource->get($id);

            return $results;

        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }


    /**
     * Wylistuj istniejace produkty
     */
    public function showVariantsProductsList(array $params = null, array $filters = null)
    {
        try {
            $resource = new ProductStock($this->client);

            if ($filters) {
                $resource->filters($filters);
            }

            if (isset($params['limit'])) {
                $resource = $resource->limit($params['limit']);
            }
            if (isset($params['page'])) {
                $resource = $resource->page($params['page']);
            }
            if (isset($params['id'])) {
                $results = $resource->get($params['id']);
            } else {
                $results = $resource->get();
            }


            return $results;

        } catch (Exception $ex) {
            die($ex->getMessage());
        }
    }

    /**
     * Loguje informacje  o przebiegu aktualizacji
     *
     * @param $str
     */
    private function log($str)
    {
        echo $str . "\n";
        if($this->fh) {
            fwrite($this->fh, $str . "\r\n");
        }
    }


    /**
     * Aktualizacja parametrow
     * @param int $id
     * @param int $stock
     * @return bool
     */
    public function updateMainStock($id, $stock = null, $deactivate = 0)
    {
        try {
            $result = 0;
            $resource = new Product($this->client);
            if (isset($stock)) {
                $data['stock']['stock'] = $stock;
                $this->log("Wymagana zmiana stanu magazynu");

            }

            if ($deactivate) {
                $data['translations']['pl_PL']['active'] = 0;
                $this->log("Wymagana deaktywacja produktu");
            }

            if ($this->change) {
                $result = $resource->put($id, $data);
                if ($result) {
                    $this->log("Produkt " . $id . " został poprawnie zaktualizowany.");
                } else {
                    $this->log("Problem z aktualizacją produktu " . $id);
                }
            } else {
                $this->log("Dry-Run. Produkt " . $id . " - Aktualizacja - stock:$stock, deactivate: $deactivate");
            }

            return $result;

        } catch (Exception $ex) {
            $this->log($ex->getMessage());
        }
    }

    /**
     * Deaktywacja wariantu ktorego stan = 0
     * @param $id
     * @param $state
     * @return bool
     */
    public function deactivateWariant($id)
    {
        try {
            $result = 0;
            $resource = new ProductStock($this->client);
            $data = array(
                'active' => false
            );

            if ($this->change) {
                $result = $resource->put($id, $data);
                if ($result) {
                    $this->log("Wariant " . $id . " został poprawnie deaktywowany.");
                } else {
                    $this->log("Problem z deaktywacją wariantu " . $id);
                }
            } else {
                $this->log("Dry-Run. Wariant " . $id . " - Deaktywacja.");
            }

            return $result;

        } catch (Exception $ex) {
            $this->log($ex->getMessage());
        }
    }

}
